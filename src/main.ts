// --------------------------
//          IMPORT
// --------------------------
import * as THREE from 'three';
import * as TWEEN from '@tweenjs/tween.js';
import * as DAT from 'dat.gui';


import disc from './assets/disc.png';
import nebula from './assets/nebula2.jpg';

import dot from './assets/dot.png';

import ParticleSystem, {
  Alpha,
  Body,
  Color,
  CrossZone,
  Emitter,
  Force,
  Life,
  Mass,
  RadialVelocity,
  RandomDrift,
  Radius,
  Rate,
  Scale,
  ScreenZone,
  Span,
  SpriteRenderer,
  Vector3D,
} from 'three-nebula';



// --------------------------
//      GLOBAL ATTIBUTES
// --------------------------
let clock = new THREE.Clock();

// Three variables
let particles: THREE.Points;
let wavyElement: THREE.Mesh;
let wavyTexture: THREE.Texture;

// Mouse position
let mouseX = 0;
let mouseY = 0;

const maxRangeX = 30; // Maximum range on the X-axis
const maxRangeY = 30; // Maximum range on the Y-axis


const windowHalfX = window.innerWidth /2;
const windowHalfY = window.innerHeight / 2;



// --------------------------
//         RENDERER
// --------------------------

const renderer = new THREE.WebGLRenderer();

renderer.setClearColor(0x000000); // ( color : Color, alpha : Float ) 

renderer.shadowMap.enabled = true;

renderer.setSize(window.innerWidth, window.innerHeight);

const parent = document.getElementById("app");
const content = document.getElementById("content");
parent?.insertBefore(renderer.domElement, content); 

// --------------------------
//          STATS
// --------------------------
// const stats = new Stats()
// stats.showPanel(0) // 0: fps, 1: ms, 2: mb, 3+: custom
// document.body.appendChild(stats.dom)


// --------------------------
//         GENERATION
// --------------------------

const generateParticles = () => {
  if (particles !== undefined)
    scene.remove(particles);

  console.log("Generating particles");
  const sprite = new THREE.TextureLoader().load(disc);
  sprite.encoding = THREE.sRGBEncoding;
  
  const geometrySpacePoints = new THREE.BufferGeometry();
  const vertices = [];
  for (let i = 0; i < options.nbPoints; i++) {
    const r1 = Math.random() <= 0.5 ? -1 : 1;
    const r2 = Math.random() <= 0.5 ? -1 : 1;
    
    const x = r1 * (options.xMax * Math.random());
    const y = r2 * (options.yMax * Math.random());
    const z = -(options.zMax * Math.random() + 5);
    
    vertices.push(x, y, z);
  }
  geometrySpacePoints.setAttribute('position', new THREE.Float32BufferAttribute(vertices, 3));
  const spacePointsMaterial = new THREE.PointsMaterial({ size: 1, sizeAttenuation: true, map: sprite, alphaTest: 0.5, transparent: true });
  spacePointsMaterial.color.setHSL(1.0, 0.3, 0.7);
  
  particles = new THREE.Points(geometrySpacePoints, spacePointsMaterial);
  scene.add(particles);
}
const createSprite = () => {
  var map = new THREE.TextureLoader().load(dot);
  var material = new THREE.SpriteMaterial({
    map: map,
    color: 0xffff00,
    blending: THREE.AdditiveBlending,
    fog: true,
  });
  return new THREE.Sprite(material);
};
// Create emiiter
// Intends to create, store and emits particles
const createEmitter = ({ colorA, colorB, camera, renderer }) => {
  const emitter = new Emitter();
  // rate : calculate particles emission's rate
  // span : get random number between a and b
  //        first parameter : numPan set number of particles
  //        second parameter : timePan whic set a time between each emission
  return emitter
    .setRate(new Rate(new Span(15, 20), new Span(0.001, 0.002)))
    .setInitializers([
      new Mass(1), // Sets the mass property 
      new Life(1), // Life span of the particle
      new Body(createSprite()), // Three object
      new Radius(20), // 
      new RadialVelocity(100, new Vector3D(1, 1, 0), 1), // Velocity, direction, Theta
    ])
    .setBehaviours([
      new Alpha(1, 0), // Starting alpha value, ending alpha value
      new Color(colorA, colorB), // Color of the particle
      new RandomDrift(10, 100, 0, 1, 10),
      new Scale(1, 0.5), // starting scale value, ending scale value
      new CrossZone(new ScreenZone(camera, renderer), 'cross'),
      //new CrossZone(new LineZone(-10, 10, 0, 0, 0, 0), 'cross'),
      // Behavior,, first screen zone has 2 parameters first is line start point x and second for y,
      // Second parameter is the cross type either is dead, bound or cross
      new Force(1, 5, -6), // Behaviour that forces particles along a specific axis.
    ])
    .emit();
};

const animateEmitters = (a, b, tha = 0, radius = 20) => {
  tha += 0.13;

  a.position.x = radius * Math.cos(tha);
  a.position.y = radius * Math.sin(tha);

  b.position.x = radius * Math.cos(tha + Math.PI / 2);
  b.position.y = radius * Math.sin(tha + Math.PI / 2);

  TWEEN.update();


  requestAnimationFrame(() => animateEmitters(a, b, tha, radius));
};
// --------------------------
//         BASICS
// --------------------------

const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerWidth, 0.1, 1000);
//const controls = new OrbitControls(camera, renderer.domElement);
scene.fog = new THREE.FogExp2( 0xc0f0ff, 0.0015 );
camera.position.set(0, 0, 60);
//controls.update();
// --------------------------
//         LIGHTS
// --------------------------

const ambientLight = new THREE.AmbientLight(0xffffff, 1);
scene.add(ambientLight);

const skyLight = new THREE.HemisphereLight(0xffffff, 0x000000, 1);
skyLight.position.set(0, -1, 4);
scene.add(skyLight);

const skyLight2 = new THREE.HemisphereLight(0xffffff, 0xff0000, 1.2);
skyLight2.position.set(0, -1, 4);
scene.add(skyLight2);


// --------------------------
//         HELPERS
// --------------------------

// const axesHelper = new THREE.AxesHelper(5);
// const gridHelper = new THREE.GridHelper(100);
// scene.add(axesHelper);
// scene.add(gridHelper);


// --------------------------
//         DAT GUI
// --------------------------

const gui = new DAT.GUI();


const options = {
  xMax: 100,
  yMax: 100,
  zMax: 100,
  nbPoints: 1000,
  colorSmoke: 0x00DDDD
}

// const xMaxOptions = gui.add(options, 'xMax', 0, 1000);
// const yMaxOptions= gui.add(options, 'yMax', 0, 1000);
// const zMaxOptions= gui.add(options, 'zMax', 0, 1000);
// const nbPointsOptions = gui.add(options, 'nbPoints', 0.1, 100000);

// gui.addColor(options, 'colorSmoke').onChange((e: any) => {
//   for (let i = 0; i < smokeParticles.length; i++) {
//     smokeParticles[i].material.color.set(e);
//   }
// })

// xMaxOptions.onChange(() => {
//   generateParticles();
// })

// yMaxOptions.onChange(() => {
//   generateParticles();
// })
// zMaxOptions.onChange(() => {
//   generateParticles();
// })
// nbPointsOptions.onChange(() => {
//   generateParticles();
// })

// --------------------------
//         OBJECTS
// --------------------------

generateParticles();

// Generate particles
const system = new ParticleSystem();
  const emitterA = createEmitter({
    colorA: '#4F1500',
    colorB: '#0029FF',
    camera,
    renderer,
  });

    
  emitterA.setPosition({'x': 90, 'y': 0, 'z': -200})
  const emitterB = createEmitter({
    colorA: '#3AA1A6',
    colorB: '#21D1DA',
    camera,
    renderer,
  });


  system
    .addEmitter(emitterA)
    .addEmitter(emitterB)
    .addRenderer(new SpriteRenderer(scene, THREE));

// new System.fromJSONAsync(system, THREE).then((system:any) => {
//   console.log(system);
// });
// generateWavyForm();

// --------------------------
//         TWEEN
// --------------------------
const f = 0.5;
let variationY = 0;
let variationZ = 0;

const coords = { 'x': 100, 'y': 200, 'z': -300 };
const startTime = TWEEN.now();
const duration = 10000;

const tween = new TWEEN.Tween(coords)
  .to({ 'x': -100, 'y': 0,'z':-200}, duration)
 // .easing(TWEEN.Easing.Circular.In)
  .onUpdate(() => {
    const currentTime = TWEEN.now(); // Get the current time
    const elapsedTimeY = (currentTime - startTime) / 500;
    const elapsedTimeZ = (currentTime - startTime)/300; // Calculate the elapsed time, influence the variation
     // Calculate the elapsed time, influence the variation
    const periodY = 2 * Math.PI * (elapsedTimeY/(1/f));
    const periodZ = 2 * Math.PI * (elapsedTimeZ/(1/f));
   
    variationY = Math.cos(f*elapsedTimeY + periodY) // 2 before; elapsed time must be a float!
    variationZ = Math.cos(f*elapsedTimeZ + periodZ) // 2 before; elapsed time must be a float!


    emitterB.setPosition({ 'x': coords.x+variationZ, 'y': coords.y+variationY*10 , 'z': coords.z+variationZ*5}) // * 10 amplitude
    
  })

// const tweenBack = new TWEEN.Tween(coords)
//   .to({ 'x': -200, 'y': 125,'z':-200}, 1000)
//   .onUpdate(() => {
//     const currentTime = TWEEN.now(); // Get the current time
//     const elapsedTime = currentTime - startTime; // Calculate the elapsed time
//     const period = 2 * Math.PI * (elapsedTime/(1/f));
    
//     variation = Math.cos(2*Math.PI*f*elapsedTime + period)

//     emitterB.setPosition({ 'x': coords.x, 'y': coords.y+variation , 'z': coords.z+variation*0.02 })
//   })

// tween.chain(tweenBack);
// tweenBack.chain(tween);
tween.start();




// --------------------------
//         ANIMATE
// --------------------------


const animate = (time:number) => {
  // delta = clock.getDelta();

  // Stars animation
  const ease = 0.05;

  const targetTranslationX = (mouseX / window.innerWidth) * maxRangeX;
  const targetTranslationY = (mouseY / window.innerHeight) * maxRangeY;

  particles.position.x += (targetTranslationX - particles.position.x) *ease;
  particles.position.y += (targetTranslationY - particles.position.y) * ease;

  
  emitterA.setRotation({ 'x': emitterA.rotation.x + 1, 'y': emitterA.rotation.y + 1 * 0.2, 'z': 1 })
  
    // particles animation
  system.update();

  animateEmitters(emitterA, emitterB);

  renderer.render(scene, camera);
}


renderer.setAnimationLoop(animate);


// --------------------------
//       WINDOW EVENTS
// --------------------------

window.addEventListener("resize", () => {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth, window.innerHeight);
});



window.addEventListener("mousemove", (e) => {
  mouseX = e.clientX - windowHalfX;
  mouseY = e.clientY - windowHalfY;
});
